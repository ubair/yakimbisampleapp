//
//  FileAttributes.m
//  yakimbiTest
//
//  Created by Ubair Hamid on 2/21/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "FileAttributes.h"


@implementation FileAttributes

@dynamic created_date;
@dynamic is_shared;
@dynamic item_id;
@dynamic last_updated_by;
@dynamic last_updated_date;
@dynamic link;
@dynamic mime_type;
@dynamic name;
@dynamic parent_id;
@dynamic path;
@dynamic path_by_id;
@dynamic share_id;
@dynamic share_level;
@dynamic shared_by;
@dynamic shared_date;
@dynamic size;
@dynamic status;
@dynamic type;
@dynamic user_id;
@dynamic attr_save_time;

@end
