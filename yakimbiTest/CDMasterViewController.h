//
//  CDMasterViewController.h
//  yakimbiTest
//
//  Created by Ubair Hamid on 2/21/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface CDMasterViewController : UITableViewController <NSFetchedResultsControllerDelegate>
{
  UIPopoverController *popoverController_;
  UIBarButtonItem *aboutButton_;
}

@property (nonatomic, retain) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

- (void) aboutButtonPressed;

@end
