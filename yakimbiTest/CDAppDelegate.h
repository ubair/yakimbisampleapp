//
//  CDAppDelegate.h
//  yakimbiTest
//
//  Created by Ubair Hamid on 2/21/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CDJsonParseHandler;

@interface CDAppDelegate : UIResponder <UIApplicationDelegate>
{
  CDJsonParseHandler *parser_;
}

@property (nonatomic, retain) UIWindow *window;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, retain) UINavigationController *navigationController;

- (void) initiateJsonParsing;
- (void) setupUI;
- (void) defineConstantSettings;
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@end
