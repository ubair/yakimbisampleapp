//
//  main.m
//  yakimbiTest
//
//  Created by Ubair Hamid on 2/21/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CDAppDelegate.h"

int main(int argc, char *argv[])
{
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([CDAppDelegate class]));
  }
}
