//
//  constants.h
//  yakimbiTest
//
//  Created by Ubair Hamid on 2/21/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "CDAppDelegate.h"

#ifndef yakimbiTest_constants_h
#define yakimbiTest_constants_h

extern BOOL IS_IPAD;
extern int IPAD_2X_MULTIPLIER;
extern float IPAD_X_MULTIPLIER;
extern float IPAD_Y_MULTIPLIER;

#define CD_APP_DELEGATE ((CDAppDelegate*)[[UIApplication sharedApplication] delegate])
#define CD_Entity_FILE_ATTRIBUTES @"FileAttributes"

#define CD_GITHUB_JSON_URL @"https://gist.github.com/raw/4680060/aac6d818e7103edfe721e719b1512f707bcfb478/sample.json"

#define CD_saved_last_rev_id @"saved_last_rev_id"

#define CD_DATA_UPDATE_LABEL_TAG 1122
#define CD_ABOUT_VIEW_TAG 1000


#endif
