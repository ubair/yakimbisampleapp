//
//  Utility.m
//  yakimbiTest
//
//  Created by Ubair Hamid on 2/21/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "Utility.h"
#import "SBJSON.h"
#import "FileAttributes.h"

@implementation Utility

+(id) getObjectFromJSONString:(NSString *) pStr {	
	
	SBJSON *parser = [[[SBJSON alloc] init] autorelease];
	pStr = [pStr stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
	return [parser objectWithString:pStr];
}

+ (NSString *) readFileAtPath:(NSString *)pPath 
{
  NSString* contents = [NSString stringWithContentsOfFile:pPath
                                                encoding:NSUTF8StringEncoding
                                                   error:NULL];
  return contents;
}

+ (NSString *) getNotNullDictValue:(id)val 
{
  if (val && val != [NSNull null] && ![val isKindOfClass:[NSNull class]] ) 
  { 
    return val;
  } 
  else 
  { 
    return @""; 
  }
}

+ (void) printFilesAttributesModel:(FileAttributes *)currFile
{
  return;
  NSLog(@"attr_save_time   \t\t\t : %f",      [currFile.attr_save_time floatValue]);
  NSLog(@"created_date     \t\t\t : %@",      currFile.created_date);
  NSLog(@"is_shared        \t\t\t : %@",      currFile.is_shared);
  NSLog(@"item_id          \t\t\t : %d",      [currFile.item_id intValue]);
  NSLog(@"last_updated_by  \t\t\t : %@",      currFile.last_updated_by);
  NSLog(@"last_updated_date\t\t\t : %@",      currFile.last_updated_date);
  NSLog(@"link             \t\t\t : %@",      currFile.link);
  NSLog(@"mime_type        \t\t\t : %@",      currFile.mime_type);
  NSLog(@"name             \t\t\t : %@",      currFile.name);
  NSLog(@"parent_id        \t\t\t : %d",      [currFile.parent_id intValue]);
  NSLog(@"path             \t\t\t : %@",      currFile.path);
  NSLog(@"path_by_id       \t\t\t : %@",      currFile.path_by_id);
  NSLog(@"share_id         \t\t\t : %@",      currFile.share_id);
  NSLog(@"share_level      \t\t\t : %d",      [currFile.share_level intValue]);
  NSLog(@"shared_by        \t\t\t : %d",      [currFile.shared_by intValue]);
  NSLog(@"shared_date      \t\t\t : %@",      currFile.shared_date);
  NSLog(@"size             \t\t\t : %f",      [currFile.size floatValue]);
  NSLog(@"status           \t\t\t : %@",      currFile.status);
  NSLog(@"type             \t\t\t : %@",      currFile.type);
  NSLog(@"user_id          \t\t\t : %d",      [currFile.user_id intValue]);
  
}

+ (NSString *) getDocumentsPath
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES); 
	NSString *documentsPath = [paths objectAtIndex:0];
	return documentsPath;
}

@end
