//
//  Utility.h
//  yakimbiTest
//
//  Created by Ubair Hamid on 2/21/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FileAttributes;

@interface Utility : NSObject

+ (id) getObjectFromJSONString:(NSString *) pStr;
+ (NSString *) readFileAtPath:(NSString *)pPath;
+ (NSString *) getNotNullDictValue:(id)val;
+ (void) printFilesAttributesModel:(FileAttributes *)currFile;
+ (NSString *) getDocumentsPath;

@end
