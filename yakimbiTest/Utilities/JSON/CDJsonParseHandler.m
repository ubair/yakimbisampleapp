//
//  CDJsonParseHandler.m
//  yakimbiTest
//
//  Created by Ubair Hamid on 2/21/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "CDJsonParseHandler.h"
#import "Utility.h"
#import "FileAttributes.h"

@implementation CDJsonParseHandler

@synthesize jsonPath = jsonPath_;

- (id) initWithFilePath:(NSString *)pPath
{
  self = [super init];
  if ( self )
  {
    jsonPath_ = [pPath retain];
    [self copyPreCacheFileToJsonPath];
  }
  return self;
}

- (void) startJsonParsingFromFile
{
  @synchronized(self)
  {
    NSString *jsonStr = [Utility readFileAtPath:jsonPath_];
    NSDictionary *jsonDict = [Utility getObjectFromJSONString:jsonStr];
    
    NSManagedObjectContext *context = [CD_APP_DELEGATE managedObjectContext];
    
    float last_rev_id = [[jsonDict objectForKey:@"last_rev_id"] floatValue];
    float saved_last_rev_id = [[NSUserDefaults standardUserDefaults] floatForKey:CD_saved_last_rev_id];
    
    if (last_rev_id > saved_last_rev_id )
    {
      NSLog(@"UPDATING DATA AND MODELS");
      
      [self deleteAllEntries];
      
      NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
      NSEntityDescription *entity = [NSEntityDescription
                                     entityForName:CD_Entity_FILE_ATTRIBUTES inManagedObjectContext:context];
      [fetchRequest setEntity:entity];
      
      NSArray *myFilesArray = [[jsonDict objectForKey:@"my_files"] objectForKey:@"content"];
      
      for ( NSDictionary *currFileAttDict in myFilesArray )
      {
        FileAttributes *currentFile = [NSEntityDescription
                                       insertNewObjectForEntityForName:CD_Entity_FILE_ATTRIBUTES
                                       inManagedObjectContext:context];
        
        ///NSLog(@"%@", currFileAttDict);
        
        currentFile.attr_save_time        = [NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]];
        currentFile.created_date          = [currFileAttDict objectForKey:@"created_date"];
        currentFile.is_shared             = [currFileAttDict objectForKey:@"is_shared"];
        currentFile.item_id               = [currFileAttDict objectForKey:@"item_id"];
        currentFile.last_updated_by       = [currFileAttDict objectForKey:@"last_updated_by"];
        currentFile.last_updated_date     = [currFileAttDict objectForKey:@"last_updated_date"];
        currentFile.link                  = [currFileAttDict objectForKey:@"link"];
        currentFile.mime_type             = [currFileAttDict objectForKey:@"mime_type"];
        currentFile.name                  = [currFileAttDict objectForKey:@"name"];
        NSString *parentID                = [Utility getNotNullDictValue:[currFileAttDict objectForKey:@"parent_id"]];
        currentFile.parent_id             = [NSNumber numberWithInt:[parentID intValue]];
        currentFile.path                  = [currFileAttDict objectForKey:@"path"];
        currentFile.path_by_id            = [currFileAttDict objectForKey:@"path_by_id"];
        currentFile.share_id              = [currFileAttDict objectForKey:@"share_id"];
        currentFile.share_level           = [NSNumber numberWithBool:[[currFileAttDict objectForKey:@"share_level"] boolValue]];
        currentFile.shared_by             = [currFileAttDict objectForKey:@"shared_by"];
        currentFile.shared_date           = [currFileAttDict objectForKey:@"shared_date"];
        currentFile.size                  = [NSNumber numberWithFloat:[[currFileAttDict objectForKey:@"size"] floatValue]];
        currentFile.status                = [currFileAttDict objectForKey:@"status"];
        currentFile.type                  = [currFileAttDict objectForKey:@"type"];
        currentFile.user_id               = [NSNumber numberWithInt:[[currFileAttDict objectForKey:@"user_id"] intValue]];
        
        NSError *error;
        if (![context save:&error]) {
          NSLog(@"Error saving data: %@", [error localizedDescription]);
        }
        
        
        NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
        FileAttributes *currFile = [fetchedObjects lastObject];
        
        [Utility printFilesAttributesModel:currFile];
      }
      
      [fetchRequest release];
      [[NSUserDefaults standardUserDefaults] setFloat:last_rev_id forKey:CD_saved_last_rev_id];
    }
    else
    {
      NSLog(@"DATA IS ALREADY UPDATED");
    }
  }
}

- (void) deleteAllEntries
{
  NSManagedObjectContext *myContext = [CD_APP_DELEGATE managedObjectContext];
  NSFetchRequest * allAttributes = [[NSFetchRequest alloc] init];
  [allAttributes setEntity:[NSEntityDescription entityForName:CD_Entity_FILE_ATTRIBUTES inManagedObjectContext:myContext]];
  [allAttributes setIncludesPropertyValues:NO]; //only fetch the managedObjectID
  
  NSError * error = nil;
  NSArray * attributes = [myContext executeFetchRequest:allAttributes error:&error];
  [allAttributes release];
  //error handling goes here
  for (NSManagedObject * attr in attributes) {
    [myContext deleteObject:attr];
  }
  NSError *saveError = nil;
  [myContext save:&saveError];
}

- (void) fetchJsonAndParseIfNeeded
{
  [NSThread detachNewThreadSelector:@selector(fetchAndSaveJsonFromURL:) toTarget:self withObject:CD_GITHUB_JSON_URL];
}

- (void) fetchAndSaveJsonFromURL:(NSString *)pURL
{
  NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
  @synchronized(self)
  {
    NSURL *url = [NSURL URLWithString:pURL];
    NSData *jsonData = [NSData dataWithContentsOfURL:url];
    [jsonData writeToFile:jsonPath_ atomically:YES];
  }
  
  [pool drain];
  [self startJsonParsingFromFile];
  
}

- (void) copyPreCacheFileToJsonPath
{
  NSError *error = nil;
  if ( ![[NSFileManager defaultManager] fileExistsAtPath:jsonPath_] )
  {
    [[NSFileManager defaultManager] copyItemAtPath:[[NSBundle mainBundle] pathForResource:@"sampleJson" ofType:@"json"] toPath:jsonPath_ error:&error];
    if (error)
      NSLog(@"Error copying precached json file");
  }
}

- (void) dealloc
{
  [jsonPath_ release];
  [super dealloc];
}
@end
