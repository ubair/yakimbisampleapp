//
//  CDJsonParseHandler.h
//  yakimbiTest
//
//  Created by Ubair Hamid on 2/21/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CDJsonParseHandler : NSObject {
  NSString *jsonPath_;
}

@property (nonatomic, retain) NSString *jsonPath;

- (id) initWithFilePath:(NSString *)pPath;
- (void) startJsonParsingFromFile;
- (void) copyPreCacheFileToJsonPath;
- (void) fetchJsonAndParseIfNeeded;
- (void) deleteAllEntries;;

@end
