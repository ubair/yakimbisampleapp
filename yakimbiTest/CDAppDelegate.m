//
//  CDAppDelegate.m
//  yakimbiTest
//
//  Created by Ubair Hamid on 2/21/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "CDAppDelegate.h"
#import "CDMasterViewController.h"
#import "Utility.h"
#import "CDJsonParseHandler.h"
#import "UIPopoverControllerIPhone.h"


@implementation CDAppDelegate

@synthesize window = _window;
@synthesize managedObjectContext = __managedObjectContext;
@synthesize managedObjectModel = __managedObjectModel;
@synthesize persistentStoreCoordinator = __persistentStoreCoordinator;
@synthesize navigationController = _navigationController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  /// defining settings for iphone or ipad environment
  [self defineConstantSettings];
  
  /// initiate json parsing calls and 
  [self initiateJsonParsing];
  
  /// setting up user interface environment 
  [self setupUI];
  
  return YES;
}

- (void) initiateJsonParsing
{
  /// parsing pre-cached json in code
  /// file is saved by the name sampleJson.json, it has last_rev_id 118, and it has a few lesser myFiles dictioanries
  
  NSString *path = [NSString stringWithFormat:@"%@/sampleJson.json",[Utility getDocumentsPath]];
  parser_ = [[CDJsonParseHandler alloc] initWithFilePath:path];
  [parser_ startJsonParsingFromFile];
  
  /// scheduling json request to server 
  [parser_ performSelector:@selector(fetchJsonAndParseIfNeeded) withObject:nil afterDelay:5.0f];
}

- (void) setupUI
{
  self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
  CDMasterViewController *masterViewController = [[[CDMasterViewController alloc] initWithNibName:@"CDMasterViewController" bundle:nil] autorelease];
  self.navigationController = [[[UINavigationController alloc] initWithRootViewController:masterViewController] autorelease];
  self.window.rootViewController = self.navigationController;
  masterViewController.managedObjectContext = self.managedObjectContext;
  
  [self.window makeKeyAndVisible];
}

- (void) defineConstantSettings
{
  if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) 
  {
    IS_IPAD = NO;
    IPAD_2X_MULTIPLIER = 1.0f;
    IPAD_X_MULTIPLIER = 1;
    IPAD_Y_MULTIPLIER = 1;
  }
  else if ( [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad )
  {
    IS_IPAD = YES;
    IPAD_2X_MULTIPLIER = 2.0f;
    IPAD_X_MULTIPLIER = 2.1333333f;
    IPAD_Y_MULTIPLIER = 2.4f;
  }
  
}

- (void)applicationWillTerminate:(UIApplication *)application
{
  // Saves changes in the application's managed object context before the application terminates.
  [self saveContext];
}

- (void)saveContext
{
  NSError *error = nil;
  NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
  if (managedObjectContext != nil)
  {
    if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error])
    {
      NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
      abort();
    } 
  }
}

#pragma mark - Core Data stack

- (NSManagedObjectContext *)managedObjectContext
{
  if (__managedObjectContext != nil)
  {
    return __managedObjectContext;
  }
  
  NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
  if (coordinator != nil)
  {
    __managedObjectContext = [[NSManagedObjectContext alloc] init];
    [__managedObjectContext setPersistentStoreCoordinator:coordinator];
  }
  return __managedObjectContext;
}

/*
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created from the application's model.
 */
- (NSManagedObjectModel *)managedObjectModel
{
  if (__managedObjectModel != nil)
  {
    return __managedObjectModel;
  }
  NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"yakimbiTest" withExtension:@"momd"];
  __managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
  return __managedObjectModel;
}

/*
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's store added to it.
 */
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
  if (__persistentStoreCoordinator != nil)
  {
    return __persistentStoreCoordinator;
  }
  
  NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"yakimbiTest.sqlite"];
  
  NSError *error = nil;
  __persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
  if (![__persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error])
  {
    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    abort();
  }    
  
  return __persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

- (NSURL *)applicationDocumentsDirectory
{
  return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (void)dealloc
{
  [parser_ release];
  [_window release];
  [__managedObjectContext release];
  [__managedObjectModel release];
  [__persistentStoreCoordinator release];
  [_navigationController release];
  [super dealloc];
}

@end
