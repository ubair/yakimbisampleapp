//
//  CDTableViewCell.h
//  yakimbiTest
//
//  Created by Ubair Hamid on 2/23/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FileAttributes;

@interface CDTableViewCell : UITableViewCell
{
  FileAttributes *model_;
  NSIndexPath *indexPath_;
}

- (id) initWithFrame:(CGRect)frame andFileAttributeModel:pModel andIndexPath:(NSIndexPath *)pRowIndex;
- (void) initializeCellView ;
- (void) updateCellWithFileAttributeModel:(FileAttributes *)pModel andIndexPath:(NSIndexPath *)pIndexPath;

@end
