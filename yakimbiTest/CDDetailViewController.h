//
//  CDDetailViewController.h
//  yakimbiTest
//
//  Created by ahmad on 2/21/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

//#import <UIKit/UIKit.h>

//@interface CDDetailViewController : UIViewController <UISplitViewControllerDelegate>
//
//@property (nonatomic, retain) id detailItem;
//
//@property (nonatomic, retain) IBOutlet UILabel *detailDescriptionLabel;
//
//@end
