//
//  CDAboutView.h
//  yakimbiTest
//
//  Created by Ubair Hamid on 2/25/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDAboutView : UIView

- (void) initialize;
- (void) okButtonCallBack ;

@end
