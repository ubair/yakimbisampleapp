//
//  CDTableViewCell.m
//  yakimbiTest
//
//  Created by Ubair Hamid on 2/23/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "CDTableViewCell.h"
#import "FileAttributes.h"

#define CD_CELL_BG_VIEW1_TAG 122
#define CD_CELL_BG_VIEW2_TAG 123
#define CD_CELL_ATTR_VALUES_TAG 124

#define CD_CELL_ATTR_NAMES_LABEL_TAG 126

@interface CDTableViewCell (private)

- (NSString *) getTextForConstantLeftColumn;
- (NSString *) getTextForCurrentFileAttributeModel;

@end

@implementation CDTableViewCell

- (id) initWithFrame:(CGRect)frame andFileAttributeModel:pModel andIndexPath:(NSIndexPath *)pIndexPath
{
  self = [super initWithFrame:frame];
  if (self) {
    self.frame = frame;
    model_ = [pModel retain];
    indexPath_ = [pIndexPath retain];
    [self initializeCellView];
  }
  return self;
}

- (void) initializeCellView 
{
  UIView *bgView1 = [[UIView alloc] initWithFrame:self.frame];
  bgView1.backgroundColor = [UIColor blackColor];
  bgView1.tag = CD_CELL_BG_VIEW1_TAG;
  self.backgroundView = bgView1;
  [bgView1 release];
  
  UIView *bgView2 = [[UIView alloc] initWithFrame:CGRectMake(self.frame.origin.x+5, self.frame.origin.y+5, self.frame.size.width-10, self.frame.size.height-10)];
  bgView2.tag = CD_CELL_BG_VIEW2_TAG;
  [bgView1 addSubview:bgView2];
  [bgView2 release];
  
  if ( indexPath_.row % 2 == 0 )
    bgView2.backgroundColor = [UIColor colorWithRed:0.7f green:0.7f blue:0.7f alpha:1.0f];
  else
    bgView2.backgroundColor = [UIColor whiteColor];  
  
  UILabel *attributeNames = [[UILabel alloc] initWithFrame:CGRectMake(5,5,(bgView2.frame.size.width-10)/4,(bgView2.frame.size.height-10))];
  attributeNames.font = [UIFont fontWithName:@"Times New Roman" size:10*IPAD_2X_MULTIPLIER];
  attributeNames.backgroundColor = [UIColor colorWithRed:1.0f green:0.5f blue:0 alpha:0.3f];///orange
  attributeNames.textAlignment = UITextAlignmentLeft;
  attributeNames.textColor = [UIColor blackColor];
  attributeNames.numberOfLines = 0;
  attributeNames.tag = CD_CELL_ATTR_NAMES_LABEL_TAG;
  attributeNames.text = [self getTextForConstantLeftColumn];
  [bgView2 addSubview:attributeNames];
  [attributeNames release];
  
  UILabel *attributeValues = [[UILabel alloc] initWithFrame:CGRectMake(5+(bgView2.frame.size.width-10)/4,5,3*(bgView2.frame.size.width-10)/4,(bgView2.frame.size.height-10))];///(2, 1, 312, 270)];
  attributeValues.font = [UIFont fontWithName:@"Times New Roman" size:10*IPAD_2X_MULTIPLIER];
  attributeValues.backgroundColor = [UIColor colorWithRed:0 green:1 blue:0 alpha:0.3f];/// green
  attributeValues.textAlignment = UITextAlignmentLeft;
  attributeValues.numberOfLines = 0;
  attributeValues.textColor = [UIColor blueColor];
  attributeValues.tag = CD_CELL_ATTR_VALUES_TAG;
  attributeValues.text = [self getTextForCurrentFileAttributeModel];
  [bgView2 addSubview:attributeValues];
  [attributeValues release];
  
}

- (void) updateCellWithFileAttributeModel:(FileAttributes *)pModel andIndexPath:(NSIndexPath *)pIndexPath
{
  [model_ release];
  model_ = [pModel retain];
  
  [indexPath_ release];
  indexPath_ = [pIndexPath retain];
  
  UIView * bgV1 = [self viewWithTag:CD_CELL_BG_VIEW1_TAG];
  UIView * bgV2 = [bgV1 viewWithTag:CD_CELL_BG_VIEW2_TAG];
  
  UILabel *attrValues = (UILabel *)[bgV2 viewWithTag:CD_CELL_ATTR_VALUES_TAG];
  attrValues.text = [self getTextForCurrentFileAttributeModel];

  if ( indexPath_.row % 2 == 0 )
    bgV2.backgroundColor = [UIColor colorWithRed:0.7f green:0.7f blue:0.7f alpha:1.0f];
  else
    bgV2.backgroundColor = [UIColor whiteColor];
}

- (NSString *) getTextForConstantLeftColumn
{
  NSString *text = nil;
  
  if ( !IS_IPAD )
  {
    text = [NSString stringWithString:
                    @"name"
                    "\ncreated_date"
                    "\nis_shared"
                    "\nitem_id"
                    "\nlast_updated_by"
                    "\nlast_updated_date"
                    "\nlink"
                    "\nmime_type"
                    "\nparent_id"
                    "\npath"
                    "\npath_by_id"
                    "\nshare_id"
                    "\nshare_level"
                    "\nshared_by"
                    "\nshared_date"
                    "\nsize"
                    "\nstatus"
                    "\ntype"
                    "\nuser_id"
                    ];
  }
  else
  {
    text = [NSString stringWithString:
            @"\tname"
            "\n\tcreated_date"
            "\n\tis_shared"
            "\n\titem_id"
            "\n\tlast_updated_by"
            "\n\tlast_updated_date"
            "\n\tlink"
            "\n\tmime_type"
            "\n\tparent_id"
            "\n\tpath"
            "\n\tpath_by_id"
            "\n\tshare_id"
            "\n\tshare_level"
            "\n\tshared_by"
            "\n\tshared_date"
            "\n\tsize"
            "\n\tstatus"
            "\n\ttype"
            "\nuser_id"
            ];
  }
  
  
  return text;
}

- (NSString *) getTextForCurrentFileAttributeModel
{
  NSString *text = nil;
  
  if ( !IS_IPAD )
  {
    text = [NSString stringWithFormat:
                    @": '%@'"
                    "\n: '%@'"
                    "\n: '%@'"
                    "\n: '%d'"
                    "\n: '%@'"
                    "\n: '%@'"
                    "\n: '%@'"
                    "\n: '%@'"
                    "\n: '%d'"
                    "\n: '%@'"
                    "\n: '%@'"
                    "\n: '%@'"
                    "\n: '%d'"
                    "\n: '%d'"
                    "\n: '%@'"
                    "\n: '%.0f'"
                    "\n: '%@'"
                    "\n: '%@'"
                    "\n: '%d'",
                    
                    model_.name, model_.created_date, model_.is_shared, [model_.item_id intValue], model_.last_updated_by, model_.last_updated_date, model_.link, model_.mime_type, [model_.parent_id intValue], model_.path, model_.path_by_id, model_.share_id, [model_.share_level intValue], [model_.shared_by intValue], model_.shared_date, [model_.size floatValue], model_.status, model_.type, [model_.user_id intValue]];
  }
  else
  {
    text = [NSString stringWithFormat:
            @"\t: '%@'"
            "\n\t: '%@'"
            "\n\t: '%@'"
            "\n\t: '%d'"
            "\n\t: '%@'"
            "\n\t: '%@'"
            "\n\t: '%@'"
            "\n\t: '%@'"
            "\n\t: '%d'"
            "\n\t: '%@'"
            "\n\t: '%@'"
            "\n\t: '%@'"
            "\n\t: '%d'"
            "\n\t: '%d'"
            "\n\t: '%@'"
            "\n\t: '%.0f'"
            "\n\t: '%@'"
            "\n\t: '%@'"
            "\n\t: '%d'",
            
            model_.name, model_.created_date, model_.is_shared, [model_.item_id intValue], model_.last_updated_by, model_.last_updated_date, model_.link, model_.mime_type, [model_.parent_id intValue], model_.path, model_.path_by_id, model_.share_id, [model_.share_level intValue], [model_.shared_by intValue], model_.shared_date, [model_.size floatValue], model_.status, model_.type, [model_.user_id intValue]];
  }
  
  
  return text;
}

/*
- (NSString *) getTextForCurrentFileAttributeModel
{
  NSString *text = [NSString stringWithFormat:
                    @"name               \t\t : '%@'"
                    "\ncreated_date       \t\t : '%@'"
                    "\nis_shared          \t\t : '%@'"
                    "\nitem_id            \t\t : '%d'"
                    "\nlast_updated_by    \t\t : '%@'"
                    "\nlast_updated_date  \t\t : '%@'"
                    "\nlink               \t\t : '%@'"
                    "\nmime_type          \t\t : '%@'"
                    "\nparent_id          \t\t : '%d'"
                    "\npath               \t\t : '%@'"
                    "\npath_by_id         \t\t : '%@'"
                    "\nshare_id           \t\t : '%@'"
                    "\nshare_level        \t\t : '%d'"
                    "\nshared_by          \t\t : '%d'"
                    "\nshared_date        \t\t : '%@'"
                    "\nsize               \t\t : '%f'"
                    "\nstatus             \t\t : '%@'"
                    "\ntype               \t\t : '%@'"
                    "\nuser_id            \t\t : '%d'",
                    
                    model_.name, model_.created_date, model_.is_shared, [model_.item_id intValue], model_.last_updated_by, model_.last_updated_date, model_.link, model_.mime_type, [model_.parent_id intValue], model_.path, model_.path_by_id, model_.share_id, [model_.share_level intValue], [model_.shared_by intValue], model_.shared_date, [model_.size floatValue], model_.status, model_.type, [model_.user_id intValue]];
  
  
  return text;
}
*/
- (void) dealloc
{
  [model_ release];
  [indexPath_ release];
  [super dealloc];
}
@end
