//
//  CDPopoverTableViewController.h
//  yakimbiTest
//
//  Created by Ubair Hamid on 2/24/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDPopoverTableViewController : UITableViewController

- (id)initWithStyle:(UITableViewStyle)style;

@end
