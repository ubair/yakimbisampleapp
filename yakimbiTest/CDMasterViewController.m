//
//  CDMasterViewController.m
//  yakimbiTest
//
//  Created by Ubair Hamid on 2/21/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "CDMasterViewController.h"
#import "CDTableViewCell.h"
#import "CDPopoverTableViewController.h"
#import "CDAboutView.h"

BOOL IS_IPAD = NO;
int IPAD_2X_MULTIPLIER = 1;
float IPAD_X_MULTIPLIER = 1.0f;
float IPAD_Y_MULTIPLIER = 1.0f;

@interface CDMasterViewController ()
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
@end

@implementation CDMasterViewController

@synthesize fetchedResultsController = __fetchedResultsController;
@synthesize managedObjectContext = __managedObjectContext;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    self.title = @"Yaskimbi Sample App";
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.separatorColor = [UIColor greenColor];
    self.view.backgroundColor = [UIColor blackColor];
    
  }
  return self;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
  [super viewDidLoad];
  aboutButton_ = [[UIBarButtonItem alloc] initWithTitle:@"About" style:UIBarButtonItemStyleBordered target:self action:@selector(aboutButtonPressed)];
  self.navigationItem.rightBarButtonItem = aboutButton_;
}

- (void) aboutButtonPressed
{
  if ( ![self.view viewWithTag:CD_ABOUT_VIEW_TAG] )
  {
    NSIndexPath *path = [NSIndexPath indexPathForRow:0  inSection:0];
    [self.tableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
    CDAboutView *aboutView = [[CDAboutView alloc] initWithFrame:CD_APP_DELEGATE.window.bounds];
    aboutView.tag = CD_ABOUT_VIEW_TAG;
    [self.view addSubview:aboutView];
    [aboutView release];
  }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
  return (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
  //  // Return YES for supported orientations
  //  if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
  //    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
  //  } else {
  //    return YES;
  //  }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
  return 273*IPAD_Y_MULTIPLIER;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
  return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
  return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  static NSString *CellIdentifier = @"Cell";
  
  CDTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
  if (cell == nil) {
    NSManagedObject *managedObject = [self.fetchedResultsController objectAtIndexPath:indexPath];
    CGRect r = CD_APP_DELEGATE.window.bounds;
    cell = [[[CDTableViewCell alloc] initWithFrame:CGRectMake(0, 0, r.size.width, 273*IPAD_Y_MULTIPLIER) andFileAttributeModel:(FileAttributes *)managedObject andIndexPath:indexPath] autorelease];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
  }
  else
  {
    [self configureCell:(UITableViewCell *)cell atIndexPath:indexPath];
  }
  return (UITableViewCell *)cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
  if (editingStyle == UITableViewCellEditingStyleDelete) {
    // Delete the managed object for the given index path
    NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
    [context deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
    
    // Save the context.
    NSError *error = nil;
    if (![context save:&error]) {
      NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
      abort();
    }
  }   
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
  // The table view should not be re-orderable.
  return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
  if (!popoverController_)
  {
    CDPopoverTableViewController *dummyTable = [[CDPopoverTableViewController alloc] initWithStyle:UITableViewStylePlain];
    popoverController_ = [[UIPopoverController alloc] initWithContentViewController:dummyTable];
    [dummyTable release];
  }
  
  [popoverController_ setPopoverContentSize:CGSizeMake(100*IPAD_2X_MULTIPLIER, 30*IPAD_2X_MULTIPLIER)];
  [popoverController_ presentPopoverFromRect:CGRectMake(0, 0, 100*IPAD_2X_MULTIPLIER, 30*IPAD_2X_MULTIPLIER) inView:cell permittedArrowDirections:UIPopoverArrowDirectionUp|UIPopoverArrowDirectionDown|UIPopoverArrowDirectionLeft|UIPopoverArrowDirectionRight animated:YES];
  
  [self performSelector:@selector(dismiss) withObject:nil afterDelay:5.0f];
}

- (void) dismiss 
{
  [popoverController_ dismissPopoverAnimated:YES];
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
  if (__fetchedResultsController != nil) {
    return __fetchedResultsController;
  }
  
  // Set up the fetched results controller.
  // Create the fetch request for the entity.
  NSFetchRequest *fetchRequest = [[[NSFetchRequest alloc] init] autorelease];
  // Edit the entity name as appropriate.
  NSEntityDescription *entity = [NSEntityDescription entityForName:@"FileAttributes" inManagedObjectContext:self.managedObjectContext];
  [fetchRequest setEntity:entity];
  
  // Set the batch size to a suitable number.
  [fetchRequest setFetchBatchSize:20];
  
  // Edit the sort key as appropriate.
  NSSortDescriptor *sortDescriptor = [[[NSSortDescriptor alloc] initWithKey:@"attr_save_time" ascending:YES] autorelease];
  NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
  
  [fetchRequest setSortDescriptors:sortDescriptors];
  
  NSFetchedResultsController *aFetchedResultsController = [[[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:@"Master"] autorelease];
  aFetchedResultsController.delegate = self;
  self.fetchedResultsController = aFetchedResultsController;
  
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    abort();
	}
  
  return __fetchedResultsController;
}    

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
  [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
  switch(type) {
    case NSFetchedResultsChangeInsert:
      [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
      break;
      
    case NSFetchedResultsChangeDelete:
      [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
      break;
  }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
  UITableView *tableView = self.tableView;
  
  switch(type) {
    case NSFetchedResultsChangeInsert:
      [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
      break;
      
    case NSFetchedResultsChangeDelete:
      [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
      break;
      
    case NSFetchedResultsChangeUpdate:
      [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
      break;
      
    case NSFetchedResultsChangeMove:
      [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
      [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]withRowAnimation:UITableViewRowAnimationFade];
      break;
  }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
  [self.tableView endUpdates];
  
  if ( ![self.view viewWithTag:CD_DATA_UPDATE_LABEL_TAG] )
  {
    
    UILabel *dataUpdated = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100*IPAD_2X_MULTIPLIER, 25*IPAD_2X_MULTIPLIER)];
    dataUpdated.text = @"Data is updated";
    dataUpdated.font = [UIFont fontWithName:@"Times New Roman" size:10*IPAD_2X_MULTIPLIER];
    dataUpdated.backgroundColor = [UIColor orangeColor];
    dataUpdated.textAlignment = UITextAlignmentCenter;
    dataUpdated.textColor = [UIColor blackColor];
    dataUpdated.tag = CD_DATA_UPDATE_LABEL_TAG;
    dataUpdated.center = self.view.center;
    [self.view addSubview:dataUpdated];
    [dataUpdated release];
    
    
    [UIView animateWithDuration:5.0 animations:^(void) {
      dataUpdated.alpha = 0.5f;
      dataUpdated.alpha = 1;
      dataUpdated.alpha = 0;
    } completion:^(BOOL finished){
      [dataUpdated removeFromSuperview];
    }
     ];
  }
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
  NSManagedObject *managedObject = [self.fetchedResultsController objectAtIndexPath:indexPath];
  [(CDTableViewCell *)cell updateCellWithFileAttributeModel:(FileAttributes *)managedObject andIndexPath:indexPath];
  
}

- (void)dealloc
{
  [__fetchedResultsController release];
  [__managedObjectContext release];
  [super dealloc];
}

@end
