//
//  CDPopoverTableViewController.m
//  yakimbiTest
//
//  Created by Ubair Hamid on 2/24/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "CDPopoverTableViewController.h"

@implementation CDPopoverTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
  self = [super initWithStyle:style];
  if (self) {
  }
  return self;
}

#pragma mark - View lifecycle

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
  return (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
  return 30*IPAD_2X_MULTIPLIER;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  static NSString *CellIdentifier = @"Cell";
  
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
  if (cell == nil) 
  {
    cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    cell.backgroundColor = [UIColor clearColor];
    cell.frame = self.view.frame;
    
    UILabel *lbl = [[UILabel alloc] initWithFrame:cell.frame];
    lbl.textAlignment = UITextAlignmentCenter;
    lbl.text = @"Row";
    lbl.font = [UIFont fontWithName:@"Times New Roman" size:12*IPAD_2X_MULTIPLIER];
    lbl.center = cell.center;
    lbl.backgroundColor = [UIColor clearColor];
    
    [cell addSubview:lbl];
    [lbl release];
  }
  
  /// No need to configure cell as it is only dummy table view , as per requirements
  
  return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  return;
}

- (void) dealloc
{
  [super dealloc];
}

@end
