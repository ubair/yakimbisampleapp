//
//  CDAboutView.m
//  yakimbiTest
//
//  Created by Ubair Hamid on 2/25/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "CDAboutView.h"
#import "CDAppDelegate.h"

@implementation CDAboutView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
      [self initialize];
    }
    return self;
}

- (void) initialize
{
  CGRect r = CD_APP_DELEGATE.window.bounds;
  UILabel *text = [[UILabel alloc] initWithFrame:r];
  text.text = @"This app is developed by Ubair Hamid for Yakimbi as a sample of my work.";
  text.font = [UIFont fontWithName:@"Times New Roman" size:15*IPAD_2X_MULTIPLIER];
  text.backgroundColor = [UIColor whiteColor];
  text.textAlignment = UITextAlignmentCenter;
  text.textColor = [UIColor blackColor];
  text.numberOfLines = 0;
  text.center = self.center;
  [self addSubview:text];
  [text release];
  
  UIButton *okButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
  okButton.frame = CGRectMake(0, 0, 60*IPAD_2X_MULTIPLIER, 30*IPAD_2X_MULTIPLIER);
  [okButton setTitle:@"Close" forState:UIControlStateNormal];
  [okButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal|UIControlStateHighlighted|UIControlStateSelected|UIControlStateDisabled];
  okButton.center = CGPointMake(self.center.x, self.center.y + self.center.y/2);
  [okButton addTarget:self action:@selector(okButtonCallBack) forControlEvents:UIControlEventTouchUpInside];
  [self addSubview:okButton];
  [self bringSubviewToFront:okButton];
  okButton.backgroundColor = [UIColor purpleColor];
}

- (void) okButtonCallBack 
{
  [self removeFromSuperview];
}

- (void) dealloc
{
  [super dealloc];
}

@end
